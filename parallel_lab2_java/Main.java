import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.MissingResourceException;

public class Main{
    public static void main(String[] args) {
        int queueCapacity = 5;
        int processToGenerate1 = 10;
        int processToGenerate2 = 15;
        
        System.out.println("\nQueue1 capacity = [" + queueCapacity + "], will be generated " + processToGenerate1 + " processes\n");
        System.out.println("\nQueue2 capacity = [" + queueCapacity + "], will be generated " + processToGenerate2 + " processes\n");

        CPUQueue cpuQueue1 = new CPUQueue(queueCapacity, "Queue1");
        CPUQueue cpuQueue2 = new CPUQueue(queueCapacity, "Queue2");


        CPUProcess cpuProcess1 = new CPUProcess(cpuQueue1, processToGenerate1);
        CPUProcess cpuProcess2 = new CPUProcess(cpuQueue2, processToGenerate2);

        CPU cpu1 = new CPU(cpuQueue1, processToGenerate1 );
        CPU cpu2 = new CPU(cpuQueue2, processToGenerate2);
        CPU cpu3 = new CPU(cpuQueue1, cpuQueue2, processToGenerate1, processToGenerate2 );

        new Thread(cpuProcess1).start();
        new Thread(cpuProcess2).start();


        new Thread(cpu1).start();
        new Thread(cpu2).start();
        new Thread(cpu3).start();
    }

}


class CPUQueue {

    private Queue<String> listQueue = new LinkedList<>();
    private String name;
    private int capacity;
    private int maxSize = 0;

    public CPUQueue(int capacity, String name) {
        this.capacity = capacity;
        this.name = name;
    }
    public int getMaxSize() {
        return maxSize;
    }
    public synchronized void put(String element) throws InterruptedException {
        while(listQueue.size() == capacity) {
            System.out.println(name +" Queue is FULL, waiting..");
            wait();
        }
        listQueue.add(element);

        if(listQueue.size()>maxSize)
            maxSize=listQueue.size();

        System.out.println( name  + " Process added, queue size = [" + listQueue.size() + "]\n");
        notify(); // notifyAll() for multiple CPU/CPUProcess threads
    }

    public synchronized String get() throws InterruptedException {
        while(isEmpty()) {
            System.out.println(name + " Queue is EMPTY, waiting..");
            wait();

        }
        String item = listQueue.remove();
        System.out.println(name + " Process removed, queue size = [" + listQueue.size() + "]");
        notify(); // notifyAll() for multiple CPU/CPUProcess threads
        return item;
    }

    public boolean isEmpty(){
        return listQueue.isEmpty();


    }
}
class CPUProcess implements Runnable {

    CPUQueue queue;
    int generateNumber;

    CPUProcess(CPUQueue q, int gN) {
        this.queue = q;
        this.generateNumber = gN;
    }

    public void run() {
        long generateDelay;
        for (int i = 0; i < generateNumber; i++) {
            int randMin = 10;
            int randMax = 40; // rand = [10,50]
            generateDelay = randMin + (int) (Math.random() * randMax);
            try {
                Thread.sleep(generateDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {

                System.out.println( Thread.currentThread().getName() + " Process generated with delay " + generateDelay);
                queue.put("New process");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println( Thread.currentThread().getName() + " No more processes. Greatest queue size was " + queue.getMaxSize());
    }
}


class CPU implements Runnable {

    CPUQueue queue1, queue2;
    int generateNumber1;
    int generateNumber2;

    CPU(CPUQueue q1, int gN1) {
        this.queue1 = q1;
        this.generateNumber1 = gN1;
    }

    CPU(CPUQueue q1, CPUQueue q2, int gN1, int gN2) {
        this.queue1 = q1;
        this.queue2 = q2;
        this.generateNumber1 = gN1;
        this.generateNumber2 = gN2;
    }

    public void run() {
        long processingTime;
        int count1 = 0;
        int count2 = 0;
        while (true) {
            int randMin = 20;
            int randMax = 80; // rand = [20,100]
            processingTime = randMin + (int) (Math.random() * randMax);
            if (queue2 == null) {
                removeFromQueue(processingTime, queue1);

            } else {
                for (int i = 0; i < generateNumber1 + generateNumber2; i++) {
                    if (!queue1.isEmpty()) {
                        removeFromQueue(processingTime, queue1);
                        count1++;


                    } else if (!queue2.isEmpty()) {
                        removeFromQueue(processingTime, queue2);
                        count2++;

                    } else
                        sleepCPU(processingTime);


                }

                break;


            }
        }
        showResults(generateNumber1, count1, generateNumber2, count2);

    }

    private void removeFromQueue(long processingTime, CPUQueue queue) {
        try {
            queue.get();
        } catch (InterruptedException e) {
            e.printStackTrace(); 
        }

        sleepCPU(processingTime);
    }

    private void sleepCPU(long processingTime) {
        System.out.println(Thread.currentThread().getName() + " CPU: Processed in time " + processingTime + "\n");
        try {
            Thread.sleep(processingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void showResults(int generateNumber1, int count1, int generateNumber2, int count2) {
        double perce1 = Math.round((double)count1 / generateNumber1 * 100);
        System.out.println(perce1 + "% operated by 3rd Processor in  Queue1" );
        double perce2 = Math.round((double) count2 / generateNumber2 * 100);
        System.out.println(perce2 + "% operated by 3rd Processor in  Queue2" );
    }
}

#ifndef CPUPROCESS_H
#define CPUPROCESS_H

#include <QObject>
#include <QThread>

#include <cpuqueue.h>
#include <cpu.h>

class CPUProcess : public QThread
{
    Q_OBJECT
    CPUQueue *queue;
    int generateNumber;

public:
    CPUProcess(CPUQueue *queue, int generateNumber);
    void run() override;
};

#endif // CPUPROCESS_H

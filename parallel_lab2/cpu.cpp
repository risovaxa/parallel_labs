#include "cpu.h"

CPU::CPU(CPUQueue *queue1, int generateNumber1)
{
    this->queue1 = queue1;
    this->generateNumber1 = generateNumber1;
}

CPU::CPU(CPUQueue *queue1, CPUQueue *queue2, int generateNumber1, int generateNumber2)
{
    this->queue1 = queue1;
    this->queue2 = queue2;
    this->generateNumber1 = generateNumber1;
    this->generateNumber2 = generateNumber2;
}

void CPU::run()
{
    size_t processingTime;
    int count1 = 0;
    int count2 = 0;
    while (true) {
        processingTime = 1;
        if (queue2 == nullptr) {
            removeFromQueue(processingTime, queue1);
        } else {
            for (int i = 0; i < generateNumber1 + generateNumber2; i++)
            {
                if (!queue1->isEmpty())
                {
                    removeFromQueue(processingTime, queue1);
                    count1++;
                } else if (!queue2->isEmpty())
                {
                    removeFromQueue(processingTime, queue2);
                    count2++;
                } else sleep(processingTime);
            }
            break;
        }
    }
    showResults(generateNumber1, count1, generateNumber2, count2);

}

void CPU::removeFromQueue(size_t processingTime, CPUQueue *queue)
{
    queue->get();
    sleep(processingTime);
    qDebug() << "CPU processed!";
}

void CPU::showResults(int generateNumber1, int count1, int generateNumber2, int count2)
{
    double result1 = double(count1) /generateNumber1*100;
    qDebug() << result1 << "% operated by CPU3 in queue1";

    double result2 = double(count2) /generateNumber2*100;
    qDebug() << result2 << "% operated by CPU3 in queue2";
}


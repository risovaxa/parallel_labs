#include <QCoreApplication>
#include <QThread>

#include <cpuqueue.h>
#include <cpu.h>
#include <cpuprocess.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int queueCapacity = 4;
    int processToGenerate1 = 15;
    int processToGenerate2 = 10;

    CPUQueue *cpuQueue1 = new CPUQueue(queueCapacity, "Queue1");
    CPUQueue *cpuQueue2 = new CPUQueue(queueCapacity, "Queue2");

    CPUProcess *cpuProcess1 = new CPUProcess(cpuQueue1, processToGenerate1);
    CPUProcess *cpuProcess2 = new CPUProcess(cpuQueue2, processToGenerate2);

    cpuProcess1->start();
    cpuProcess2->start();

    CPU *cpu1 = new CPU(cpuQueue1, processToGenerate1 );
    CPU *cpu2 = new CPU(cpuQueue2, processToGenerate2);
    CPU *cpu3 = new CPU(cpuQueue1, cpuQueue2, processToGenerate1, processToGenerate2 );

    cpu1->start();
    cpu2->start();
    cpu3->start();

    return a.exec();
}

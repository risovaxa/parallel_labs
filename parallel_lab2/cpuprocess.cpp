#include "cpuprocess.h"


CPUProcess::CPUProcess(CPUQueue *queue, int generateNumber)
{
    this->queue = queue;
    this->generateNumber = generateNumber;
}

void CPUProcess::run()
{
    size_t generateDelay;
    for (int i = 0; i < generateNumber; i++) {
        generateDelay = 0;
        sleep(generateDelay);
        qDebug() << currentThreadId() << " Process generated";
        queue->put("New process");
    }
    qDebug() << currentThreadId() << " No more processes. Greatest queue size was: " + queue->getMaxSize();
}


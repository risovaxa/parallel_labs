#ifndef CPUQUEUE_H
#define CPUQUEUE_H

#include <QThread>
#include <QQueue>
#include <QMutex>
#include <QDebug>

#include <Windows.h>

class CPUQueue : public QObject
{
    Q_OBJECT
    QQueue<QString>listQueue;
    QString name;
    QMutex mtx;
    int capacity;
    int max_size = 0;

public:
    CPUQueue(int capacity, QString name);
    int     getMaxSize  () const;
    void    put         (QString element);
    QString get         ();
    bool    isEmpty     ();

signals:
    void notify();
};

#endif // CPUQUEUE_H

#ifndef CPU_H
#define CPU_H

#include <QThread>
#include <QObject>

#include "cpuqueue.h"

class CPU : public QThread
{
    Q_OBJECT
    CPUQueue *queue1, *queue2;
    int generateNumber1;
    int generateNumber2;
public:
    CPU(CPUQueue *queue1, int generateNumber1);
    CPU(CPUQueue *queue1, CPUQueue *queue2, int generateNumber1, int generateNumber2);

    void run() override;
    void removeFromQueue(size_t processingTime, CPUQueue *queue);
    void showResults(int generateNumber1, int count1, int generateNumber2, int count2);


};

#endif // CPU_H

#include "cpuqueue.h"

int CPUQueue::getMaxSize() const
{
    return max_size;
}

void CPUQueue::put(QString element)
{
    while(!mtx.tryLock())
    {
        while(listQueue.size() == capacity)
        {
            qDebug() << "queue is FULL, waiting...";
            Sleep(DWORD(1000));
        }
        listQueue.append(element);
        if(listQueue.size() > max_size) max_size = listQueue.size();
        qDebug() << "Process added, queue size: " << listQueue.size();

        mtx.unlock();
    }
}

QString CPUQueue::get()
{
    QString item;
    while(!mtx.tryLock())
    {
        while(isEmpty())
        {
            qDebug() << "queue is EMPTY, waiting...";
            Sleep(DWORD(1000));
        }
        item = listQueue.dequeue();
        qDebug() << name << " Process removed, queue size: " << listQueue.size();
        mtx.unlock();
    }
    return item;
}

bool CPUQueue::isEmpty()
{
    return listQueue.isEmpty();
}

CPUQueue::CPUQueue(int capacity, QString name)
{
    this->capacity = capacity;
    this->name = name;
}

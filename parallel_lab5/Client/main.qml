import QtQuick 2.5
import QtQuick.XmlListModel 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: applicationWindow
    width: 596
    height: 600
    visible: true
    title: qsTr("Client")

    Image{
        x: -2
        y: 0
        width: 600
        fillMode: Image.PreserveAspectFit
        source: "qrc:/6bc37b8f6c33a2093f14c014c42cf45b.jpg"
    }
    Text{
        width: 598
        height: 600
        color: "white"
        text: ClientCPP.message
        font.pointSize: 40
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

    }


}






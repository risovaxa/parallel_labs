#ifndef CLIENT_H
#define CLIENT_H

#include <QQuickView>
#include <QtNetwork>
#include <QtCore>

class QUdpSocket;

class Client : public QQuickView
{
    Q_OBJECT
    Q_PROPERTY(QString message READ message WRITE setMessage NOTIFY sigMessageChanged)
public:
    explicit Client(QWindow *parent = nullptr);

    QString message() const;
    void setMessage(const QString &value);

signals:
    void sigMessageChanged(QString message);

private slots:
    void processPendingDatagrams();

private:
    QString messageReceived;
    QUdpSocket *udpSocket = nullptr;
};
#endif // CLIENT_H

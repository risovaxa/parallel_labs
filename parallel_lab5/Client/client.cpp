#include "client.h"

Client::Client(QWindow *parent)
    : QQuickView(parent)
{
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(1010, QUdpSocket::ShareAddress);
    QObject::connect(udpSocket, &QUdpSocket::readyRead, this, &Client::processPendingDatagrams);

}

void Client::processPendingDatagrams()
{
    QByteArray datagram;
    while (udpSocket->hasPendingDatagrams()) {
        datagram.resize(int(udpSocket->pendingDatagramSize()));
        udpSocket->readDatagram(datagram.data(), datagram.size());
        setMessage(datagram.constData());
    }
}

QString Client::message() const
{
    return messageReceived;
}

void Client::setMessage(const QString &value)
{
    messageReceived = value;
    emit sigMessageChanged(messageReceived);
}

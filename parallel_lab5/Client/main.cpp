#include <QCoreApplication>
#include <QQmlContext>
#include <QApplication>
#include <QObject>

#include <client.h>

int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication a(argc, argv);

    Client* client = new Client(qobject_cast<QWindow*>(a.parent()));
    client->rootContext()->setContextProperty("ClientCPP", client);

    client->setSource(QUrl(QStringLiteral("qrc:/main.qml")));

    a.exec();
}

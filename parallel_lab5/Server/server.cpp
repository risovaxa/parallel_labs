#include <QtWidgets>
#include <QtNetwork>
#include <QtCore>

#include "server.h"

Server::Server(QWindow *parent)
    : QQuickView(parent)
{
    udpSocket = new QUdpSocket(this);

    QObject::connect(this, &Server::sigSendMessage, this, &Server::broadcastDatagram);
}

void Server::sendMessage()
{
    qDebug() << "Sending message...";
    emit sigSendMessage(portsToSend);
}

void Server::addPort(QString port)
{
    portsToSend.append(port.toInt());
    qDebug() << portsToSend;
}

void Server::removePort(QString port)
{
    int index = portsToSend.indexOf(port.toInt());
    portsToSend.removeAt(index);
    qDebug() << portsToSend;
}

void Server::broadcastDatagram(QList<int> ports)
{
    QByteArray datagram = QByteArray("MESSAGE ") + QByteArray::number(messageNo);
    foreach (auto port, ports) {
        udpSocket->writeDatagram(datagram, QHostAddress::Broadcast, port);
        ++messageNo;
    }

}

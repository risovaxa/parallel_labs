#ifndef SERVER_H
#define SERVER_H


#include <QQuickView>
#include <QTimer>

class QUdpSocket;

class Server : public QQuickView
{
    Q_OBJECT

public:
    explicit Server(QWindow *parent = nullptr);

    Q_INVOKABLE void sendMessage();
    Q_INVOKABLE void addPort(QString port);
    Q_INVOKABLE void removePort(QString port);

signals:
    void sigSendMessage(QList<int>ports);

private slots:
    void broadcastDatagram(QList<int>ports);

private:
    QList<int> portsToSend;
    QUdpSocket *udpSocket = nullptr;
    QTimer timer;
    int messageNo = 1;
};

#endif // SERVER_H

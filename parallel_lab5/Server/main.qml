import QtQuick 2.5
import QtQuick.XmlListModel 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: applicationWindow
    width: 596
    height: 600
    visible: true
    title: qsTr("Server")

    Image{
        x: -2
        y: 0
        width: 600
        fillMode: Image.PreserveAspectFit
        source: "qrc:/6bc37b8f6c33a2093f14c014c42cf45b.jpg"
    }

    XmlListModel {
        id: xmlModel
        source: "qrc:/clients.xml"
        query: "/rss/client"

        XmlRole { name: "title"; query: "title/string()" }
        XmlRole { name: "port"; query: "port/string()" }
    }

    ListView {
        x: 10
        y: 20
        width: 560
        height: 400
        anchors.horizontalCenter: parent.horizontalCenter
        model: xmlModel
        //        delegate: Text { text: title + " " + port }

        delegate: Item {
            id: element
            x: 10
            y: 60
            width: 600
            height: 54
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter

            Text {
                id: textTitle
                x:20
                color: "white"
                width: 150
                height: 24
                text: title
                anchors.verticalCenter: parent.verticalCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
            }

            Text {
                id: textPort
                x: 180
                y: 0
                color: "white"
                width: 150
                height: 24
                text: port
                anchors.verticalCenter: parent.verticalCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
            }

            CheckBox {
                id: checkBox
                x: 310
                y: 19
                anchors.verticalCenter: parent.verticalCenter
                onCheckedChanged: {
                    if(checkBox.checked == false) {
                        console.log(port);
                        ServerCPP.removePort(port);
                    }
                    if(checkBox.checked == true) {
                        console.log(port);
                        ServerCPP.addPort(port);
                    }
                }
            }
        }

    }

    Button {
        id: submit
        x: 31
        y: 540
        Text {
            anchors.fill: parent
            text: qsTr("Send Alert")
            font.bold: true
            renderType: Text.NativeRendering
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family: "Arial"
            font.pointSize: 10

            color: submit.pressed ? "lightgrey" : "#ffffffff"
        }

        style: ButtonStyle {
            background: Rectangle {
                id: cancel_style
                implicitWidth: 97
                implicitHeight: 23
                color: "#00000000"
                radius: 5
                border.color: submit.pressed ? "lightgrey" : "#ffffffff"
                border.width: 2
            }
        }
        onClicked: {
            ServerCPP.sendMessage();

        }
    }


}






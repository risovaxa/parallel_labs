#include <QCoreApplication>
#include <QQmlContext>
#include <QApplication>
#include <QObject>

#include <server.h>


int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication a(argc, argv);

    Server* server = new Server(qobject_cast<QWindow*>(a.parent()));
    server->rootContext()->setContextProperty("ServerCPP", server);

    server->setSource(QUrl(QStringLiteral("qrc:/main.qml")));
    return a.exec();
}

#ifndef AWORKER_H
#define AWORKER_H

#include <QDebug>

#include <vector>
#include <future>
class AWorker
{
public:
    explicit AWorker(std::vector<int> vec);

    virtual std::vector<int> operation();
    virtual void run();

    std::vector<int>::iterator     get_begin()   const;
    std::vector<int>::iterator     get_end()     const;
    std::vector<int>               get_result_vector();
//    std::future<std::vector<int>>  get_result_future();
private:
    std::vector<int> work_vec;
    std::vector<int> result_vec;

    std::future<std::vector<int>> result;

    std::vector<int>::iterator begin;
    std::vector<int>::iterator end;
};

#endif // AWORKER_H

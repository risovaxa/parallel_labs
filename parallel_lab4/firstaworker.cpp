#include "firstaworker.h"


std::vector<int> FirstAWorker::operation()
{
    qDebug() << "FIRST WORKER OPERATION START";
    std::vector<int> result;
    int max = 0;
    for(auto p = get_begin(); p != get_end(); p++)
    {
        if(*p > max){
            max = *p;
        }
    }

    qDebug() << "MAX: " << max;

    for(auto p = get_begin(); p != get_end(); p++)
    {
        if(*p > max * 0.6){
            result.push_back( *p );
        }
    }
    sort(result.begin(), result.end());
    return result;
}

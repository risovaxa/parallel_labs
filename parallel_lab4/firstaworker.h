#ifndef FIRSTAWORKER_H
#define FIRSTAWORKER_H

#include "aworker.h"

class FirstAWorker : public AWorker
{
public:
    using AWorker::AWorker;
    std::vector<int> operation() override;
};

#endif // FIRSTAWORKER_H

#ifndef SECONDAWORKER_H
#define SECONDAWORKER_H

#include "aworker.h"

class SecondAWorker : public AWorker
{
public:
    using AWorker::AWorker;
    std::vector<int> operation() override;
};

#endif // SECONDAWORKER_H

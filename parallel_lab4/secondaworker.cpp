#include "secondaworker.h"


std::vector<int> SecondAWorker::operation()
{
    qDebug() << "SECOND WORKER OPERATION START";
    std::vector<int> result;
    for(auto p = get_begin(); p != get_end(); p++)
    {
        if(*p % 2 == 0)
            result.push_back(*p);
    }
    sort(result.begin(), result.end());
    return result;
}

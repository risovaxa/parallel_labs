#include <QCoreApplication>
#include <QDebug>
#include <vector>
#include <future>

#include <ctime>
#include <random>
#include <algorithm>
#include <iostream>
#include <omp.h>
#include <set>

#include "firstaworker.h"
#include "secondaworker.h"

//template <typename T>
//std::vector<int> first_vec_operation(T begin, T end)
//{
////    /typename T::difference_type len = end - begin;

//    std::vector<int> result;
//    int max = 0;
//    for(auto p = begin; p != end; p++)
//    {
//        if(*p > max){
//            max = *p;
//        }
//    }
//    for(auto p = begin; p != end; p++)
//    {
//        if(*p > max * 0.6){
//            result.push_back( *p );
//        }
//    }
//    result.s
//    return result;
//}



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::vector<int> vec1;
    for(int i = 0; i < 10; i++){
        std::uniform_int_distribution<> vec_elem(000, 100);
        std::random_device rd;
        std::mt19937 gen(rd());
        vec1.push_back(vec_elem(gen));
    }

    std::vector<int> vec2;
    for(int i = 0; i < 10; i++){
        std::uniform_int_distribution<> vec_elem(000, 100);
        std::random_device rd;
        std::mt19937 gen(rd());
        vec2.push_back(vec_elem(gen));
    }
    qDebug() << vec1;
    qDebug() << vec2;
    FirstAWorker wrk1(vec1);
    SecondAWorker wrk2(vec2);

    wrk1.run();
    wrk2.run();

    std::vector<int> vec1result = wrk1.get_result_vector();
    std::vector<int> vec2result = wrk2.get_result_vector();

//    std::set<int> set1;
//    foreach (const auto &elem, vec1result) {
//        set1.insert(elem);
//    }

//    std::set<int> set2;
//    foreach (const auto &elem, vec2result) {
//        set2.insert(elem);
//    }
    std::vector<int> intersection;
    std::set_intersection(vec1result.begin(), vec1result.end(),
                          vec2result.begin(), vec2result.end(),
                          std::back_inserter(intersection));

    qDebug() << "INTERSECTION: " << intersection;
    return a.exec();
}

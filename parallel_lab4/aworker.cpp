#include "aworker.h"



AWorker::AWorker(std::vector<int> vec) : work_vec(vec)
{
    begin = work_vec.begin();
    end = work_vec.end();
}

std::vector<int> AWorker::operation()
{
    //DO SMTH
    qDebug() << "BASE CLASS";
    return std::vector<int>();
}

void AWorker::run()
{
    result = std::async(std::launch::async, &AWorker::operation, this);
}

std::vector<int>::iterator AWorker::get_begin() const
{
    return begin;
}

std::vector<int>::iterator AWorker::get_end() const
{
    return end;
}

std::vector<int> AWorker::get_result_vector()
{
    qDebug() << "RESULT VECTOR INCOMING";
    result.wait();
    return result.get();
}

//std::future<std::vector<int> > AWorker::get_result_future()
//{
//    qDebug() << "RESULT FUTURE";
//    result.wait();
//    return result;
//}




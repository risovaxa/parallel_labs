#include <QCoreApplication>
#include <QVector>
#include <QDebug>
#include <QCryptographicHash>

#include <algorithm>
#include <iostream>
#include <ctime>
#include <random>
#include <omp.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::vector<int> vec;

    std::atomic_int size(0);
    std::atomic_int atomicSum;

    int position_min = 0;
    int position_max = 0;

    int size_na = 0;

    int max = 0;
    int min = 0;

    int max_test = 0;
    int min_test = 0;



    for(int i = 0; i < 100; i++){
        std::uniform_int_distribution<> vec_elem(0000000, 10000000);
        std::random_device rd;
        std::mt19937 gen(rd());
        vec.push_back(vec_elem(gen));
    }
        qDebug() << vec;

    //expected MAX and MIN
    foreach (auto elem, vec) {
        if(elem > max_test){
            max_test = elem;
        }
    }
    foreach (auto elem, vec) {
        if(elem < min_test){
            min_test = elem;
        }
    }



    int sizeeee = 0;

    #pragma omp parallel
    {
        foreach (int elem, vec)
        {
            sizeeee++;
            #pragma omp single
            {
              //FIND SIZE
              size++;

              #pragma omp atomic update
              size_na++;

//              qDebug() << omp_get_thread_num();
//              qDebug() << size << elem;

              //FIND HASHSUM
              atomicSum +=   elem;

              int oldValue;
              int newValue;

              do {
                  oldValue = atomicSum;
                  newValue = oldValue ^ elem;
              } while(!atomicSum.compare_exchange_strong(oldValue, newValue));

              //FIND MIN-MAX

              if(elem > max){
                  #pragma omp atomic read
                  max = elem;
                  //#pragma omp atomic read
                  position_max = size;
              }

              if(elem < min){
                  #pragma omp atomic read
                  min = elem;
                  //#pragma omp atomic read
                  position_min = size;
              }
            }
        }
    }
    qDebug() << "MAX EXPECTED: " << max_test << "MAX REAL: " << max << "POSITION MAX: " << position_max;
    qDebug() << "MIN EXPECTED: " << min_test << " MIN REAL: "<< min << "POSITION MIN: " << position_min;
    qDebug() << "VEC SIZE EXPECTED: " << vec.size() << "VEC SIZE REAL: " << size_na;
    qDebug() << "HASH SUM REAL: " << atomicSum;
    qDebug() << sizeeee;
    return a.exec();
}

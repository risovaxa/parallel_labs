#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QDebug>
#include <QThread>
#include <QElapsedTimer>

class Thread : public QThread
{
    Q_OBJECT
public:
    Thread(int _low, int _high, std::vector<int> _vec, QString name_) {
        low = _low;
        //qDebug() << "LOW: " << low;
        high = _high;
        //qDebug() << "HIGH: " << high;
        vec = _vec;
        //qDebug() << "VEC: " << vec;
        name = name_;
    }

public slots:
    int getMax() const;

private:
    int max;
    int low;
    int high;
    std::vector<int>vec;
    QString name;

    void calc()
    {
        max = 0;
        for(int i = low; i < high; i++){
            if(vec[i] > max){
                max = vec[i];
            }
        }
    }

    void run()
    {
        QElapsedTimer timer;
        timer.start();

        qDebug() << "Calc started";

        calc();

//        qDebug() << "CURRENT THREAD: " << name
//                 << "MAX: " << max
//                 << "TIME: " << timer.elapsed() << "\n";
    }
};

#endif // WORKER_H

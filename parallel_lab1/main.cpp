#include <QCoreApplication>
#include <QDebug>
#include <QThread>

#include <ctime>
#include <random>
#include <algorithm>
#include <iostream>

#include "worker.h"

bool lol = true;
bool lolSingle = true;
int maxElem;

void maxElemSingleThread(std::vector<int>vec)
{
    QElapsedTimer timerSingle;
    timerSingle.start();
    qDebug() << "SINGLE THREAD APP STARTED \n";

    Thread* threadS = new Thread(0, vec.size(), vec, QString("Single"));
    threadS->start();

    while(lolSingle)
    {
        while(threadS->isFinished())
        {
            lolSingle = false;

            break;
        }
    }
    qDebug()<<"MAX ELEM: " << threadS->getMax() << "\nTIME SPENT: " << timerSingle.elapsed()
             << "\nSINGLE THREAD APP FINISHED";
}


void maxElemMultiThread(std::vector<int> vec)
{
    qDebug() << "----------------------------" << "\n\nMULTI THREAD APP STARTED\n";
    int n = 13;
    QElapsedTimer timerMulti;
    timerMulti.start();

    std::vector<Thread*> threadpool;

    for(int i = 0; i < n - 1; i++)
    {
        QString name = QString("MULTI") + QString();
        Thread* thread = new Thread(vec.size()/n*i, vec.size()/n*(i+1), vec, name);
        thread->start();
        threadpool.push_back(thread);
    }
    Thread* thread = new Thread(vec.size()/n*(n-2), vec.size(), vec, QString("MultiLast"));
    thread->start();
    threadpool.push_back(thread);

    while(lol)
    {
        while(true)
        {
            //qDebug() << "threads finished";
            std::vector<int> maxEl;

            for(int i = 0; i < n; i++)
            {
                threadpool[i]->wait();
                maxEl.push_back(threadpool[i]->getMax());

            }
            std::vector<int>::iterator result;

            result = std::max_element(maxEl.begin(), maxEl.end());
            qDebug() << "MAX_ELEM:" << maxEl.at(std::distance(maxEl.begin(), result));


            lol = false;
            break;
        }
    }
    qDebug() << "TIME SPENT: "<< timerMulti.elapsed()
             << "\nMULTI THREAD APP FINISHED";

    //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::vector<int> vec;

    for(int i = 0; i < 333333; i++){
        std::uniform_int_distribution<> vec_elem(0000000, 10000000);
        std::random_device rd;
        std::mt19937 gen(rd());
        vec.push_back(vec_elem(gen));
    }
    qDebug() << vec;

    maxElemSingleThread(vec);

    maxElemMultiThread(vec);

    a.exec();
}

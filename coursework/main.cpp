#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>
#include <QElapsedTimer>

#include <omp.h>

#include "plog/Log.h"
#include "plog/Logger.h"
#include "plog/Init.h"
#include "plog/WinApi.h"
#include "plog/Appenders/ColorConsoleAppender.h"
#include "plog/Formatters/TxtFormatter.h"
#include "plog/Appenders/RollingFileAppender.h"

#include "datamodel.h"

#ifdef QT_DEBUG
    static plog::RollingFileAppender<plog::TxtFormatter> fileAppender("VeePN.log", 100000, 1);
    static plog::ColorConsoleAppender<plog::TxtFormatter> consoleAppender;
#else
    static plog::RollingFileAppender<plog::TxtFormatter> fileAppender("VeePN.log", 100000, 1);
    static plog::ColorConsoleAppender<plog::TxtFormatter> consoleAppender;
#endif


const double A = 15.0;
const double B = 13.0;
const double a = 0.5;
const double b = 0.7;


double F(long double tau)
{
    return B - 10 * a * tau;
}

double bottomLimit(double tau)
{
    double result = pow(A * pow(F(tau), -0.2)
                             - (b / (24 * a)) * F(tau), 2.0);
//    LOG_DEBUG << "w(0, t) = " << result;
    return result;
}

double topLimit(double tau)
{
    long double result= pow(1 / F(tau) + A * pow(F(tau), -0.2)
                             - (b / (24 * a)) * F(tau), 2.0);
//    LOG_DEBUG << "w(1, t) = " << result;
    return result;
}

double tZeroRowFunction(long double x)
{
    double result = pow(x * x / F(0.0) + A * pow(F(0.0), -0.2)
                             - (b / (24 * a)) * F(0.0), 2.0);
//    LOG_DEBUG << "w(x, 0) = " << result;
    return result;
}

double correctFunc(long double x, long double tau)
{
    double result = pow(x * x / F(tau) + A * pow(F(tau), -0.2)
                             - (b / (24 * a)) * F(tau), 2.0);
//    LOG_DEBUG << "w(x, t) = " << result;
    return result;
}

double aproxFunc(double rightTop, double leftTop, double centerTop, double tau, double h)
{
    double result = centerTop + tau * (0.5 * a * pow(centerTop, 0.5) * pow(((rightTop - leftTop) / 2 * h), 2)
                                                + a * pow(centerTop, 0.5) * ((leftTop - 2 * centerTop + rightTop)/(pow(h, 2)))
                                                + b * pow(centerTop, 0.5));
//    LOG_DEBUG << "w(i, k+1) = " << result;
    return result;
}

int main(int argc, char *argv[])
{
    #ifdef QT_DEBUG
        plog::init(plog::debug, &consoleAppender).addAppender(&fileAppender);
    #else
        plog::init(plog::debug, &consoleAppender).addAppender(&fileAppender);
    #endif

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    LOG_DEBUG << "APP INIT";

    DataModel *aproximate_data_model_ = new DataModel;
    DataModel *direct_data_model_     = new DataModel;

    LOG_DEBUG << "STARTED CREATING WINDOW";
    QQuickView view_;
    view_.setSource(QUrl(QStringLiteral("qrc:/main.qml")));

    view_.rootContext()->setContextProperty("AproximateDataModel", aproximate_data_model_);
    view_.rootContext()->setContextProperty("DirectDataModel", direct_data_model_);

    LOG_DEBUG << "WINDOW CREATED SUCCESSFULLY";
    QElapsedTimer timer;
    timer.start();

    const int numOfStepsX = 10;
    const int numOfStepsT = 900;

    double stepX = 1.0 / (numOfStepsX - 1);
    double stepT = 1.0 / (numOfStepsT - 1);

    double correctMatrix[numOfStepsT][numOfStepsX] = { 0.0 };
    double aproxMatrix[numOfStepsT][numOfStepsX] = { 0.0 };

    double currentX = 0.0;
    double currentT = 0.0;

    for (int i = 0; i < numOfStepsT; ++i) {
        currentX = 0.0;
        for (int j = 0; j < numOfStepsX; ++j) {
            correctMatrix[i][j] = correctFunc(currentX, currentT);
            currentX += stepX;
        }
        currentT += stepT;
    }

    currentX = 0.0;
    for (int i = 0; i < numOfStepsX; ++i) {
        aproxMatrix[0][i] = correctFunc(currentX, 0.0);
        currentX += stepX;

    }

    currentT = 0.0;
    for (int i = 0; i < numOfStepsT; ++i) {
        aproxMatrix[i][0] = bottomLimit(currentT);
        aproxMatrix[i][numOfStepsX - 1] = topLimit(currentT);
        currentT += stepT;
    }


    for (int i = 1; i < numOfStepsT; ++i) {
        #pragma omp parallel for
        for (int j = 1; j < numOfStepsX; ++j) {
            aproxMatrix[i][j] = aproxFunc(aproxMatrix[i - 1][j + 1], aproxMatrix[i - 1][j - 1],
                                          aproxMatrix[i - 1][j],stepT, stepX);
        }
    }

    for (int i = 0; i < numOfStepsT; i++) {
        for (int j = 0; j < numOfStepsX; j++) {
            double i_double = (double)i/numOfStepsT;
            double j_double = (double)j/numOfStepsX;
            LOG_DEBUG << "i: " << i_double;
            LOG_DEBUG << "j: " << j_double;
            LOG_DEBUG << "w_aprox: " << aproxMatrix[i][j];
            LOG_DEBUG << "w_correct: " << correctMatrix[i][j];
            aproximate_data_model_->addNode(j_double, i_double, aproxMatrix[i][j]);
            direct_data_model_->addNode(j_double, i_double, correctMatrix[i][j]);
        }
    }

    double maxError = 0.0;
    double maxRelativeError = 0.0;
    int mjE, miE = 0;

    double threadError = 0.0;
    double relativeError = 0.0;
    int jE, iE = 0;

    #pragma omp parallel
    {
        for (int i = 0; i < numOfStepsT; ++i) {
        #pragma omp parallel for
            for (int j = 0; j < numOfStepsX; ++j) {
                double temp = abs(correctMatrix[i][j] - aproxMatrix[i][j]);
                double tempRelative = abs((correctMatrix[i][j] - aproxMatrix[i][j]) / aproxMatrix[i][j]) * 100;
                if (temp > threadError) {
                    threadError = temp;
                    relativeError = tempRelative;
                    iE = i;
                    jE = j;
                }
            }
        }
    }


    if (threadError > maxError) {
        maxError = threadError;
        maxRelativeError = relativeError;
        mjE = jE;
        miE = iE;
    }
    LOG_FATAL << "Thread error: " << threadError;
    LOG_FATAL << "Thread relative error: " << relativeError;



    LOG_DEBUG << "Max error is: " << maxError << " at " << miE << " : " << mjE;
    LOG_DEBUG << "Max realtive error is: " << maxRelativeError << " at " << miE << " : " << mjE;

    qDebug() << "ELAPSED TIME: " << timer.elapsed();

    return app.exec();
}

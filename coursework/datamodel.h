#ifndef DATAMODEL_H
#define DATAMODEL_H


#include <QAbstractListModel>
#include <QList>
#include <QHash>

#include "plog/Log.h"

struct DataNode
{
    double W    = 0.0;
    double X    = 0.0;
    double T    = 0.0;
};

class DataModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles
    {
        XAxis,
        TAxis,
        WAxis
    };

    explicit DataModel(QObject *parent = nullptr);
    ~DataModel();

    int getNameRole() const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    void addNode(double X, double T, double W);

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<DataNode*> data_;

};


#endif // DATAMODEL_H

#include "datamodel.h"

DataModel::DataModel(QObject *parent)
    : QAbstractListModel(parent)
{ }

DataModel::~DataModel()
{
    qDeleteAll(data_);
}

QVariant DataModel::headerData(int section, Qt::Orientation orientation, int role) const { return QVariant(); }

bool DataModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role) { return false; }

int DataModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return data_.size();
}

QVariant DataModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    switch (role) {
        case XAxis:
            return QVariant(data_[index.row()]->X);
        case TAxis:
            return QVariant(data_[index.row()]->T);
        case WAxis:
            return QVariant(data_[index.row()]->W);
        default:
            return QVariant();
    }
}

bool DataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid() && role == Qt::EditRole) {
        LOG_ERROR << "setData, Index is not valid!";
        return false;
    }

    if (data(index, role) != value) {
        switch (role) {
            case XAxis:
                return false; /// This property can not be set
            case TAxis:
                return false; /// This property can not be set
            case WAxis:
                return  false; /// This property can not be set
            default:
                return false;
        }

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}


Qt::ItemFlags DataModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        LOG_DEBUG <<  "Invoke flags: index is not vlid!";
        return Qt::NoItemFlags;
    }
    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}


bool DataModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    endInsertRows();
    return true;
}


bool DataModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    endRemoveRows();
    return true;
}


QHash<int, QByteArray> DataModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[XAxis]     = "XAxis";
    roles[TAxis]     = "TAxis";
    roles[WAxis]     = "WAxis";

    return roles;
}


void DataModel::addNode(double X, double T, double W)
{
    DataNode *node = new DataNode;
    node->T = T;
    node->W = W;
    node->X = X;

    beginInsertRows(QModelIndex(), data_.size(), data_.size());

    data_.append(node);

    endInsertRows();

    QModelIndex idx = createIndex(data_.size(), 0, static_cast<void*>(nullptr));
    emit dataChanged(idx, idx);
}

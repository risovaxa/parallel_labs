import QtQuick 2.12
import QtQuick.Window 2.12
import QtDataVisualization 1.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Coursework")

    ColorGradient {
        id: layerOneGradient
        ColorGradientStop { position: 0.0; color: "pink" }
        ColorGradientStop { position: 1.0; color: "blue" }
    }

    ColorGradient {
        id: layerTwoGradient
        ColorGradientStop { position: 0.0; color: "blue" }
        ColorGradientStop { position: 1.0; color: "white" }
    }

    Surface3D {
        width: parent.width
        height: parent.height
        Surface3DSeries {
            name: "Aproximate Solution"
            baseColor: "blue"
//            baseGradient: layerTwoGradient
            itemLabelFormat: "omega at (@xLabel X, @zLabel T): @yLabel ModelName: " + name
            flatShadingEnabled: false
            drawMode: Surface3DSeries.DrawSurface
            ItemModelSurfaceDataProxy {
                itemModel: AproximateDataModel
                // Mapping model roles to surface series rows, columns, and values.
                rowRole: "TAxis"
                columnRole: "XAxis"
                yPosRole: "WAxis"
            }
        }
        Surface3DSeries {
            name: "Exact Solution"
            baseColor: "yellow"
//            baseGradient: layerOneGradient
            itemLabelFormat: "omega at (@xLabel X, @zLabel T): @yLabel ModelName: " + name
            flatShadingEnabled: false
            drawMode: Surface3DSeries.DrawSurface
            ItemModelSurfaceDataProxy {
                itemModel: DirectDataModel
                // Mapping model roles to surface series rows, columns, and values.
                rowRole: "TAxis"
                columnRole: "XAxis"
                yPosRole: "WAxis"
            }
        }
    }
}
